/** -*- js -*-
 * Define a set of template paths to pre-load
 * Pre-loaded templates are compiled and cached for fast access when rendering
 * @return {Promise}
 */
export const preloadHandlebarsTemplates = async function () {
  return loadTemplates([
    "systems/dune/templates/part/domain.html",
    "systems/dune/templates/part/enemy.html",
    "systems/dune/templates/part/trait.html"
  ]);
};
